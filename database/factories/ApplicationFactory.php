<?php

namespace Database\Factories;

use App\Enums\ApplicationStatus;
use App\Enums\ContributionWay;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Application>
 */
final class ApplicationFactory extends Factory
{

    public function withCompanyState(): array
    {
        return [
            'company_name' => $this->faker->company(),
            'company_siret' => 12345678901234,
            'company_legal_address' => $this->faker->optional(0.7)->address(),
            'company_work_address' => $this->faker->optional()->address(),
        ];
    }

    public function withoutCompanyState(): array
    {
        return [
            'company_name' => null,
            'company_siret' => null,
            'company_legal_address' => null,
            'company_work_address' => null,
        ];
    }

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $name = $this->faker->lastName();
        $firstname = $this->faker->firstName();
        return [
            'name' => $name,
            'firstname' => $firstname,
            'address' => $this->faker->address(),
            'postcode' => $this->faker->postcode(),
            'city' => $this->faker->city(),
            'email' => preg_replace('/[[:^print:]]/', '', $firstname).'@example.org',
            'mobile' => $this->faker->phoneNumber(),

            ...($this->faker->boolean() ? $this->withCompanyState() : $this->withoutCompanyState()),

            'birth_year' => $this->faker->optional()->year(),
            'gender' => $this->faker->optional()->word(),

            'mail_local' => $this->faker->boolean(),

            'contribution' => $this->faker->randomFloat(2, 0, 50),
            'contribution_way' => $this->faker->randomElement(array_map(fn($unitEnum) => $unitEnum->value, ContributionWay::cases())),

            'sepa_full_name' => $this->faker->name(),
            'sepa_address' => $this->faker->address(),
            'sepa_postcode' => $this->faker->postcode(),
            'sepa_city' => $this->faker->city(),
            'sepa_country' => 'France',
            'iban' => 'GB82 WEST 1234 5698 7654 32',
            'bic' => $this->faker->swiftBicNumber(),
            'made_at' => $this->faker->city(),
            'signature' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABkCAMAAAAL3/3yAAAAllBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6C80qAAAAMXRSTlMABA8ZMUhNeoCMlJmira+ztbq8vb6/wMLDxsfIycrLzM7P0dPU1djd5ejr7/L6+/z9dD9ENQAAAVRJREFUeNrt2cFSgzAUQNGUlopW1FoDVbRqUUsxKeX/f86dxYWZ0Dgy+u7ZZnfnPciAUgAAAAAAAAAAAICT1jQgFrEGdq8J6W2dOmK9U+uLOnHt6CuBumzsOJxVBOoYtSPH6bgdk+ggts7jakaig6R2Hr/whO9I19zC/O+kBbF+6gJPLGIRi1jEIhaxiEUsYg0eC8QaItYp/XxdG3NCBT+L9pYIvgytvK0MDTzNjZnz/vPdwRWXBV+FlB0M/8sclVbKVTb4L3Nuy0jKszn0L3PS5oLeZNVTWKtE0ms/tQFbpGW1UqosA+ZK2leKyC6PfbZLmyul1LLJjqycK3km2+3kd/f3T8ua/kPyaCOZsVTecxWnxb6+EvvhoNcq3tT7Yqoky5oH76FaKOkuPeYlvhM/VN2hedPfet7sdpsLMn060w7nMYEAAAAAAAAAAAAAAAAA/AMfQ18aKZVKhl8AAAAASUVORK5CYII=',

            'status' => ApplicationStatus::NEW,

            'applicant_comment' => $this->faker->optional()->realText(500),

            'created_at' => now(),
        ];
    }
}
