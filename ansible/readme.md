Execution sur un serveur avec ansible d'un cron 


```shell
ansible-pull -U https://USERDEPLOYGITLAB:PASSWORD@gitlab.com/solidaires-informatique/adhesion.git/ -C BRANCHNAMEIFNEED ansible/update_adhesion.yml  --vault-password-file VaultFile --only-if-changed

```

Pour ajouter une clée vaut chiffré par exemple
 ( avec le fichier VaultFile qui contient le mot de passe du vault mais on peut le faire en stdin ect ...)


```shell
ansible-vault encrypt_string  -n SIRENE_API_KEY --vault-password-file VaultFile TACLEEAPIPOURCOULERLESBATEAUXPATRONAUXETCOLLONIAUXAVECTASIRENE
```

On obtient ca 


```yaml
SIRENE_API_KEY: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          38386335623661346561396436616633336636336431626439653938383830663436626165396537
          3162646136346135643236336165336463383537356663630a393039626634393235333964326661
          66373533363864613137656432633935386130363435646436653036616439386333656233333334
          3965313132386432340a333137356230393530623063653236656436313235323336306166366438
          66343932333336613439626632383161376364663966306533353036333539336231383330326366
          66656261636161316335313065643832666166633830303864306536303837386563323930356265
          393337303063643538343635666537336461

```

Et on a l'ajoute généralement dans les groups-vars ou les hosts vars suivant le cas 


