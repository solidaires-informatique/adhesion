<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title ?? config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
</head>
<body class="font-sans antialiased">
<x-banner/>

<div class="min-h-screen bg-gray-100">
    @livewire('navigation-menu')

    <main class="p-6">
        @if(session('error_message'))
            <div class="max-w-4xl m-auto">
                <p class="bg-red-200 rounded p-4 text-center m-2">
                    {{ session('error_message') }}
                </p>
            </div>
        @endif
        @if(session('success_message'))
            <div class="max-w-4xl m-auto">
                <p class="bg-green-200 rounded p-4 text-center m-2">
                    {{ session('success_message') }}
                </p>
            </div>
        @endif
            {{ $slot }}
    </main>
</div>

@stack('modals')

@livewireScripts
</body>
</html>
