<div class="my-4 px-6">
    <h3 class="font-bold">Bulletin d'adhésion</h3>
    <dl class="grid md:grid-cols-3 grid-cols-2 gap-4 sm:px-6">
        <livewire:application.element
            :title="__('validation.attributes.name')"
            :content="$application->name"
        />
        <livewire:application.element
            :title="__('validation.attributes.firstname')"
            :content="$application->firstname"
        />
        @if($showRestrictedInformation)
            <livewire:application.element
                :title="__('validation.attributes.address')"
                :content="$application->address"
            />
        @endif
        <livewire:application.element
            :title="__('validation.attributes.postcode')"
            :content="$application->postcode"
        />
        <livewire:application.element
            :title="__('validation.attributes.city')"
            :content="$application->city"
        />
        <livewire:application.element
            :title="__('validation.attributes.email')"
            :content="$application->email"
        />
        <livewire:application.element
            :title="__('validation.attributes.mobile')"
            :content="$application->mobile"
        />
        @if($application->birth_year)
            <livewire:application.element
                :title="__('validation.attributes.birth_year')"
                :content="$application->birth_year"
            />
        @endif
        @if($application->gender)
            <livewire:application.element
                :title="__('validation.attributes.gender')"
                :content="$application->gender"
            />
        @endif

        @if($showRestrictedInformation)
        <livewire:application.element
            :title="__('validation.attributes.contribution')"
            :content="number_format($application->contribution, 2, ',', '')"
        />
        <livewire:application.element
            :title="__('validation.attributes.contribution_way')"
            :content='__("form.contribution_way_value.{$application->contribution_way->value}")'
        />
        @endif

        <hr class="md:col-span-3 col-span-2">
        @if($application->company_name || $application->company_legal_address || $application->company_work_address)
            <livewire:application.element
                :title="__('validation.attributes.company_name')"
                :content="$application->company_name"
            />
            @if($application->company_siret)
                <livewire:application.element
                    title="SIRET"
                    :content="$application->company_siret"
                />
            @endif
            @if($application->company_legal_address)
                <livewire:application.element
                    :title="__('validation.attributes.company_legal_address')"
                    :content="$application->company_legal_address"
                />
            @endif
            @if($application->company_work_address)
                <livewire:application.element
                    :title="__('validation.attributes.company_work_address')"
                    :content="$application->company_work_address"
                />
            @endif
        @else
            <livewire:application.element
                :title="__('validation.attributes.company_name')"
                content="Aucune information"
            />
        @endif
        <hr class="md:col-span-3 col-span-2">

        @if($application->applicant_comment)
            <div class="col-span-3">
                <dt class="text-gray-600">{{ __('validation.attributes.comment') }}</dt>
                <dd>{{ $application->applicant_comment }}</dd>
            </div>
        @endif

        @if($application->id !== null)
            <div class="md:col-span-3 col-span-2"></div>
            <livewire:application.element
                title="Liste mail section territoriale"
                :content="$application->mail_local ? 'Oui' : 'Non'"
            />
        @endif
    </dl>

    @if($showRestrictedInformation && $application->contribution_way === \App\Enums\ContributionWay::SEPA)
        <div>
            <h3 class="font-bold mt-8">Mandat SEPA</h3>
            <dl class="grid md:grid-cols-3 grid-cols-2 gap-4 sm:px-6">
                <livewire:application.element
                    :title="__('validation.attributes.full_name')"
                    :content="$application->sepa_full_name"
                />
                <livewire:application.element
                    :title="__('validation.attributes.address')"
                    :content="$application->sepa_address"
                 />
                <livewire:application.element
                    :title="__('validation.attributes.postcode')"
                    :content="$application->sepa_postcode"
                 />
                <livewire:application.element
                    :title="__('validation.attributes.city')"
                    :content="$application->sepa_city"
                 />
                <livewire:application.element
                    :title="__('validation.attributes.country')"
                    :content="$application->sepa_country"
                 />
                <livewire:application.element
                    :title="__('validation.attributes.iban')"
                    :content="$application->iban"
                 />
                <livewire:application.element
                    :title="__('validation.attributes.bic')"
                    :content="$application->bic"
                 />
                <livewire:application.element
                    :title="__('validation.attributes.made_at')"
                    :content="$application->made_at"
                 />
                <livewire:application.element
                    :title="__('form.current_date')"
                    :content="($application->id === null ? now() : $application->created_at)->format('d/m/Y')"
                 />
                <div>
                    <dt class="text-gray-600">{{ __('validation.attributes.signature') }}</dt>
                    <img src="{{ $application->signature }}" alt="" width="150">
                </div>
            </dl>
        </div>
    @endif
</div>
