<?php

declare(strict_types=1);

namespace App\Livewire\Dashboard;

use App\Enums\OptionEnum;
use App\Livewire\HasPrivateUserProperty;
use App\Models\Option;
use Illuminate\Support\Carbon;
use Livewire\Component;

final class General extends Component
{
    use HasPrivateUserProperty;

    public bool $hasEnabledTwoFactorAuthentication;
    public bool $shouldUpdateWelcomeSessionLink;

    public function mount(): void
    {
        $this->hasEnabledTwoFactorAuthentication = $this->user->hasEnabledTwoFactorAuthentication();
        $this->shouldUpdateWelcomeSessionLink = Carbon::parse(Option::get(OptionEnum::WELCOME_SESSION_LINK)->updated_at)->lessThan(now()->subMonths(6));
    }
}
