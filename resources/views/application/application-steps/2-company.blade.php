<div class="form__section">
    <div class="form__input-group">
        <div class="form__input-group__element">
            <label class="form__input-group__element__label">
                <input wire:model.live="employee" name="employee" class="mr-2 leading-tight" type="checkbox">
                <span>{{ __("form.have_employer") }}</span>
            </label>
        </div>
    </div>

    @if($employee)
        <div class="form__input-group">
            <div class="form__input-group__element">
                <label class="form__input-group__element__label">
                    <input wire:model.live="noSiret" name="noSiret" class="mr-2 leading-tight" type="checkbox">
                    <span>Je ne connais pas le numéro SIRET de mon employeur</span>
                </label>
                <p class="form__input-group__element__instruction">
                    Cochez cette case si vous ne connaissez pas le n° de SIRET ou si le numéro n’est pas reconnu.
                </p>
            </div>
        </div>

        @if(!$noSiret)
            <div class="form__input-group">
                <div class="form__input-group__element">
                    <label class="form__input-group__element__label" for="company_siret">
                        Numéro SIRET
                    </label>
                    <input
                        wire:model.blur="application.company_siret"
                        name="company_siret"
                        type="text"
                        id="company_siret"
                        class="form__input-group__element__input @error('application.company_siret') errored @enderror"
                    >
                    @error('application.company_siret') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
                </div>
            </div>

            @if($application->company_name)
                    <div class="form__input-group">
                        <div class="form__input-group__element">
                            <p>{{ __('validation.attributes.company_name') }} : {{ $application->company_name }}</p>
                        </div>
                        <div class="form__input-group__element">
                            <p>{{ __('validation.attributes.company_legal_address') }} : {{ $application->company_legal_address }}</p>
                        </div>
                        <div class="form__input-group__element">
                            <p>{{ __('validation.attributes.company_work_address') }} : {{ $application->company_work_address }}</p>
                        </div>
                    </div>
            @endif
        @else
            <div class="form__input-group">
                <div class="form__input-group__element">
                    <label class="form__input-group__element__label" for="company_name">
                        {{ __('validation.attributes.company_name') }}
                    </label>
                    <input
                        wire:model.blur="application.company_name"
                        name="company_name"
                        type="text"
                        id="company_name"
                        autocomplete="organization"
                        @if(!$noSiret) disabled @endif
                        class="form__input-group__element__input @error('application.company_name') errored @enderror"
                    >
                    @error('application.company_name') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
                </div>

                <div class="form__input-group__element">
                    <label class="form__input-group__element__label" for="company_legal_address">
                        {{ __('validation.attributes.company_legal_address') }}
                    </label>
                    <input
                        wire:model.blur="application.company_legal_address"
                        name="company_legal_address"
                        id="company_legal_address"
                        type="text"
                        autocomplete="street-address"
                        @if(!$noSiret) disabled @endif
                        class="form__input-group__element__input @error('application.company_legal_address') errored @enderror"
                    >
                    @error('application.company_legal_address') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
                </div>

                <div class="form__input-group__element">
                    <label class="form__input-group__element__label" for="company_work_address">
                        {{ __('validation.attributes.company_work_address') }}
                    </label>
                    <input
                        wire:model.blur="application.company_work_address"
                        name="company_work_address"
                        id="company_work_address"
                        type="text"
                        autocomplete="street-address"
                        @if(!$noSiret) disabled @endif
                        class="form__input-group__element__input @error('application.company_work_address') errored @enderror"
                    >
                    @if($noSiret)
                        <p class="form__input-group__element__instruction">
                            Si différente de celle du siège (pour savoir à quelle section territoriale vous rapprocher).
                        </p>
                    @endif
                    @error('application.company_work_address') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
                </div>
            </div>
        @endif
    @endif
</div>
