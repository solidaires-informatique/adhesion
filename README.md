# adhesion

Plateforme d'adhésion de [Solidaires Informatique](https://solidairesinformatique.org), en ligne à [adhesion.solidairesinformatique.org](https://adhesion.solidairesinformatique.org/).

## Développement

Pour simplifier les commandes, on considère qu'un alias `sail` existe dans votre configuration.

```bash
alias sail='[ -f sail ] && sh sail || sh vendor/bin/sail'
```

Si ce n'est pas le cas, remplacez `sail` par `vendor/bin/sail`.

### Installation pour le développement

Prérequis :

- [docker](https://docs.docker.com/get-started/get-docker/)

```bash
# Installation des dépendances PHP (dont Sail)
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v "$(pwd):/var/www/html" \
    -w /var/www/html \
    laravelsail/php83-composer:latest \
    composer install --ignore-platform-reqs

# Lancement initial du projet
cp .env.example .env
sail up -d
sail artisan key:generate

# Installation des dépendances frontend
sail composer install
sail npm install

# Initialisation de la base de données
sail artisan migrate:fresh --seed

# Compilation des assets
sail npm run build
```

Le projet est disponible à [http://localhost/](http://localhost/)
L'interface pour voir les mails envoyés est sur [http://localhost:8025/](http://localhost:8025/)

Pour arrêter les containers :

```bash
sail stop # ou sail down pour les arrêter et les supprimer
```

Une fois installé, pour lancer le projet il suffit d'exécuter :

```bash
sail up
sail npm run dev
```

### Vérifications automatiques du code

- Règles de style avec `php-cs-fixer` (`sail composer tests:lint`)
- Analyse statique avec `phpstan` (`sail composer tests:static`)
- Tests d'architecture avec `Pest` (`sail composer tests:arch`)
- Suite de tests PHP avec `Pest` (`sail composer tests:suite`)

Vous pouvez lancez toutes les vérifications en une seule commande :

```bash
sail composer tests
```

### API SIRENE

Pour la récupération des informations de l'entreprise avec le numéro siret :

1. Suivez les instructions sur https://gitlab.com/solidaires-informatique/sirene pour récupérer une clef API
2. Remplir `SIRENE_API_KEY` dans le fichier `.env`.
3. `sail restart`

## Déploiement

Le serveur Web doit diriger toutes les requêtes vers `public/index.php`.

Besoins:

-   PHP >= 8.3
-   BCMath PHP Extension
-   Ctype PHP Extension
-   cURL PHP Extension
-   DOM PHP Extension
-   Fileinfo PHP Extension
-   JSON PHP Extension
-   Mbstring PHP Extension
-   OpenSSL PHP Extension
-   PCRE PHP Extension
-   PDO PHP Extension
-   Tokenizer PHP Extension
-   XML PHP Extension

Pour que les tâches programmées soit exécutées la cron suivante est nécessaire :

```shell
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```

Le site est déployé automatiquement avec [ansible](./ansible/readme.md).
