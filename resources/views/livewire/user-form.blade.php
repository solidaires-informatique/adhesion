<div class="w-full max-w-4xl mx-auto mt-2">
    <header class="mb-2 text-center">
        <h1 class="text-4xl">@if($this->creation === false){{ $user->pseudo }}@else{{ __('dashboard.users.create') }}@endif</h1>
    </header>

    <div class="mt-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white px-4 py-5 sm:px-6 rounded">
                <form wire:submit="submit" method="POST" class="form">
                    @csrf

                    <div class="form__input-group">
                        <div class="form__input-group__element">
                            <label class="form__input-group__element__label" for="pseudo">
                                {{ __('Pseudo') }}
                            </label>
                            <input
                                wire:model="user.pseudo"
                                id="pseudo"
                                type="text"
                                required
                                class="form__input-group__element__input @error('user.pseudo') errored @enderror"
                            >
                            @error('user.pseudo') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
                        </div>
                    </div>

                    <div class="form__input-group">
                        <div class="form__input-group__element">
                            <label class="form__input-group__element__label" for="email">
                                {{ __('Email') }}
                            </label>
                            <input
                                wire:model="user.email"
                                id="email"
                                type="text"
                                class="form__input-group__element__input @error('user.email') errored @enderror"
                            >
                            @error('user.email') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
                        </div>
                    </div>

                    <div class="form__input-group">
                        <div class="form__input-group__element form__input-group__element_1/2">
                            <label class="form__input-group__element__label" for="role">
                                Role
                            </label>
                            <select
                                wire:model="user.role"
                                id="role"
                                required
                                class="form__input-group__element__select"
                            >
                                @foreach(\App\Enums\Role::cases() as $role)
                                    <option value="{{ $role }}" >
                                        {{ __("dashboard.role.$role->value") }}
                                    </option>
                                @endforeach
                            </select>
                            @error('user.role') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
                        </div>
                    </div>

                    <div class="flex flex-wrap gap-1">
                        <button type="submit" class="form__submit @error('submit') form__submit_error @enderror">
                            <span>@if($this->creation) Créer @else Mettre à jour @endif</span>
                        </button>
                        @error('submit')
                        <p class="form__input-group__element__error">{{ $message }}</p>
                        @enderror

                        @if($this->creation === false)
                            <button class="form__submit form__submit_error" wire:click.prevent="$set('confirmingResetPassword', true)">
                                Réinitialiser le mot de passe
                            </button>
                        @endif

                        @if($this->creation === false)
                            <button class="form__submit form__submit_error" wire:click.prevent="$set('confirmingDelete', true)">
                                Supprimer le compte
                            </button>
                        @endif
                    </div>
                </form>

                <x-confirmation-modal wire:model.live="confirmingResetPassword">
                    <x-slot name="title">
                        Réinitialiser le mot de passe
                    </x-slot>

                    <x-slot name="content">
                        Êtes-vous sûr de vouloir réinitialiser le mot de passe de ce compte ?
                    </x-slot>

                    <x-slot name="footer">
                        <x-secondary-button wire:click="$toggle('confirmingResetPassword')" wire:loading.attr="disabled">
                            Non
                        </x-secondary-button>

                        <x-danger-button class="ml-2" wire:click="resetPassword" wire:loading.attr="disabled">
                            Réinitialiser
                        </x-danger-button>
                    </x-slot>
                </x-confirmation-modal>

                <x-confirmation-modal wire:model.live="confirmingDelete">
                    <x-slot name="title">
                        Supprimer le compte
                    </x-slot>

                    <x-slot name="content">
                        Êtes-vous sûr de vouloir supprimer ce compte ? ({{ $user->pseudo }})
                    </x-slot>

                    <x-slot name="footer">
                        <x-secondary-button wire:click="$toggle('confirmingDelete')" wire:loading.attr="disabled">
                            Non
                        </x-secondary-button>

                        <x-danger-button class="ml-2" wire:click="delete" wire:loading.attr="disabled">
                            Supprimer
                        </x-danger-button>
                    </x-slot>
                </x-confirmation-modal>
            </div>
        </div>
    </div>
</div>
