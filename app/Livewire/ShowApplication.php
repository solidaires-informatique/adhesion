<?php

declare(strict_types=1);

namespace App\Livewire;

use App\Enums\ApplicationStatus;
use App\Models\Application;
use Illuminate\Support\Facades\Log;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

final class ShowApplication extends Component
{
    use HasPrivateUserProperty;
    use HasTextFields;

    public Application $application;
    public bool $canDone;
    public string $doneName;
    public bool $canReject;
    public bool $canUndo;
    public bool $confirmUndo = false;

    public function mount(): void
    {
        $this->canDone = $this->showDoneAction();
        $this->doneName = match ($this->application->status) {
            ApplicationStatus::NEW => __('dashboard.actions.validate'),
            ApplicationStatus::VALIDATED => __('dashboard.actions.complete'),
            ApplicationStatus::COMPLETED, ApplicationStatus::READY => __('dashboard.actions.done'),
            default => '',
        };
        $this->canReject = $this->showRejectAction();
        $this->canUndo = $this->showUndoAction();
    }

    public function reject(): void
    {
        if (!$this->canReject) {
            $this->abortCant('reject');
        }

        $this->application->status = ApplicationStatus::REJECTED;
        $this->application->update();

        Log::notice("Application {$this->application->id} rejected.");
        $this->dispatch('move-application', applicationId: $this->application->id);
    }

    public function done(): void
    {
        if (!$this->canDone) {
            $this->abortCant('done');
        }

        if ($this->application->status === ApplicationStatus::VALIDATED) {
            $this->redirect(route('dashboard.application.complete', [$this->application]));

            return;
        }

        $this->application->status = $this->application->status->next();
        $this->application->update();

        Log::notice("Application {$this->application->id} {$this->application->status->value}.");
        $this->dispatch('move-application', applicationId: $this->application->id);
    }

    public function undo(): void
    {
        if (!$this->canUndo) {
            $this->abortCant('undo');
        }

        $this->confirmUndo = false;
        $this->canUndo = false;
        $this->application->status = $this->application->status->prev();
        $this->application->update();

        Log::notice("Undo status change on application {$this->application->id}, put back to {$this->application->status->value}.");
        $this->dispatch('move-application', applicationId: $this->application->id);
    }

    private function showDoneAction(): bool
    {
        if (\in_array($this->application->status, [ApplicationStatus::REJECTED, ApplicationStatus::WELCOMED], true)) {
            return false;
        }

        if ($this->application->status === ApplicationStatus::VALIDATED) {
            return $this->user->can('complete', Application::class);
        }

        return true;
    }

    private function showRejectAction(): bool
    {
        if (!\in_array($this->application->status, [ApplicationStatus::NEW, ApplicationStatus::VALIDATED], true)) {
            return false;
        }

        return $this->user->can('reject', Application::class);
    }

    private function showUndoAction(): bool
    {
        return match ($this->application->status) {
            ApplicationStatus::REJECTED => $this->user->can('reject', Application::class),
            ApplicationStatus::WELCOMED => true,
            default => false,
        };
    }

    private function abortCant(string $action): never
    {
        Log::warning("User {$this->user->id} can't {$action} application {$this->application->id} with status {$this->application->status->name}");

        abort(Response::HTTP_FORBIDDEN);
    }
}
