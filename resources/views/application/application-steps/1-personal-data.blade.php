<div class="form__section">
    <h2 class="form__section__instruction">
        Informations obligatoires. Tous les champs ci-dessous doivent être remplis.
    </h2>
    <div class="form__input-group">
        <div class="form__input-group__element form__input-group__element_1/2">
            <label class="form__input-group__element__label" for="name">
                {{ __('validation.attributes.name') }}
            </label>
            <input
                wire:model.blur="application.name"
                id="name"
                name="name"
                type="text"
                autocomplete="family-name"
                required
                class="form__input-group__element__input @error('application.name') errored @enderror"
            >
            @error('application.name') <p class="form__input-group__element__error">{{ $message }}</p> @enderror

        </div>
        <div class="form__input-group__element form__input-group__element_1/2">
            <label class="form__input-group__element__label" for="firstname">
                {{ __('validation.attributes.firstname') }}
            </label>
            <input
                wire:model.blur="application.firstname"
                name="firstname"
                id="firstname"
                type="text"
                required
                autocomplete="given-name"
                class="form__input-group__element__input @error('application.firstname') errored @enderror"
            >
            @error('application.firstname') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>
    </div>

    <div class="form__input-group">
        <div class="form__input-group__element form__input-group__element_1/2">
            <label class="form__input-group__element__label" for="postcode">
                {{ __('validation.attributes.postcode') }}
            </label>
            <input
                wire:model.blur="application.postcode"
                name="postcode"
                type="text"
                id="postcode"
                required
                autocomplete="postal-code"
                class="form__input-group__element__input @error('application.postcode') errored @enderror"
            >
            @error('application.postcode') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>

        <div class="form__input-group__element form__input-group__element_1/2">
            <label class="form__input-group__element__label" for="city">
                {{ __('validation.attributes.city') }}
            </label>
            <input
                wire:model.blur="application.city"
                name="city"
                type="text"
                id="city"
                required
                autocomplete="city"
                class="form__input-group__element__input @error('application.city') errored @enderror"
            >
            @error('application.city') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>
    </div>

    <div class="form__input-group">
        <div class="form__input-group__element">
            <label class="form__input-group__element__label" for="address">
                {{ __('validation.attributes.address') }}
            </label>
            <input
                wire:model.blur="application.address"
                name="address"
                type="text"
                id="address"
                required
                autocomplete="street-address"
                class="form__input-group__element__input @error('application.address') errored @enderror"
            >
            @error('application.address') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>
    </div>

    <div class="form__input-group">
        <div class="form__input-group__element">
            <label class="form__input-group__element__label" for="email">
                {{ __('validation.attributes.email') }}
            </label>
            <input
                wire:model.blur="application.email"
                name="email"
                type="email"
                id="email"
                required
                autocomplete="email"
                class="form__input-group__element__input @error('application.email') errored @enderror"
            >
            <p class="form__input-group__element__instruction">
                Evitez les courriels professionnels, votre entreprise pourrait lire votre activité syndicale.
            </p>
            @error('application.email') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>
    </div>

    <div class="form__input-group">
        <div class="form__input-group__element">
            <label class="form__input-group__element__label" for="mobile">
                {{ __('validation.attributes.mobile') }}
            </label>
            <input
                wire:model.blur="application.mobile"
                name="mobile"
                type="tel"
                id="mobile"
                required
                autocomplete="tel-national"
                class="form__input-group__element__input @error('application.mobile') errored @enderror"
            >
            @error('application.mobile') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>
    </div>
</div>

<div class="form__section">
    <h2 class="form__section__instruction">
        {{ __('Informations facultatives. Nous les demandons pour des raisons statistiques internes.') }}
    </h2>
    <div class="form__input-group">
        <div class="form__input-group__element form__input-group__element_1/2">
            <label class="form__input-group__element__label" for="birth_year">
                {{ __('validation.attributes.birth_year') }}
            </label>
            <input
                wire:model.blur="application.birth_year"
                name="birth_year"
                id="birth_year"
                type="text"
                autocomplete="bday-year"
                class="form__input-group__element__input @error('application.birth_year') errored @enderror"
            >
            @error('application.birth_year') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>

        <div class="form__input-group__element form__input-group__element_1/2">
            <label class="form__input-group__element__label" for="gender">
                {{ __('validation.attributes.gender') }}
            </label>
            <input
                wire:model.blur="application.gender"
                name="gender"
                id="gender"
                type="text"
                autocomplete="sex"
                class="form__input-group__element__input @error('application.gender') errored @enderror"
            >
            <p class="form__input-group__element__instruction">
                (H / F / NB / ...)
            </p>
            @error('application.gender') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>
    </div>
</div>
