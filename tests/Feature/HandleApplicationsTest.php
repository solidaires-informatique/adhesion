<?php

declare(strict_types=1);

use App\Enums\ApplicationStatus;
use App\Enums\ContributionWay;
use App\Enums\Role;
use App\Livewire\ApplicationComplete;
use App\Livewire\ShowApplication;
use App\Livewire\ShowApplications;
use App\Mail\WelcomeNotification;
use App\Models\User;
use Database\Factories\ApplicationFactory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Livewire\Livewire;

beforeEach(function () {
    Storage::fake('nextcloud');
});

test('show application information', function (Role $role): void {
    $application = createApplication();
    $this->actingAs(User::factory()->create(['role' => $role]));

    Livewire::test(ShowApplication::class, ['application' => $application])
        ->assertSeeText($application->name)
        ->assertSeeText($application->city)
        ->assertStatus(200)
    ;
})->with([Role::BOARD, Role::TREASURY, Role::ADMIN]);

test('can reject new application', function (Role $role): void {
    $application = createApplication(['status' => ApplicationStatus::NEW]);
    $this->actingAs(User::factory()->create(['role' => $role]));

    $list = Livewire::test(ShowApplications::class, ['status' => [ApplicationStatus::NEW->value]])
        ->assertSeeText($application->address)
    ;

    Livewire::test(ShowApplication::class, ['application' => $application])
        ->call('reject')
        ->assertDispatched('move-application', applicationId: $application->id)
    ;

    expect($application->refresh()->status)->toEqual(ApplicationStatus::REJECTED);

    $list->call('removeApplication', $application->id)
        ->assertDontSeeText($application->address)
    ;
})->with([Role::BOARD, Role::TREASURY, Role::ADMIN]);

test('can validate application', function (Role $role): void {
    $application = createApplication(['status' => ApplicationStatus::NEW], User::factory()->create(['role' => $role]));

    Mail::fake();
    Livewire::test(ShowApplication::class, ['application' => $application])
        ->assertSee(__('dashboard.actions.validate'))
        ->call('done')
        ->assertDispatched('move-application', applicationId: $application->id)
        ->assertHasNoErrors()
    ;
    expect($application->refresh()->status)->toEqual(ApplicationStatus::VALIDATED);
    Mail::assertNothingSent();
})->with([Role::TREASURY, Role::ADMIN, Role::BOARD]);

test('can complete application', function (Role $role): void {
    $application = createApplication([
        'status' => ApplicationStatus::VALIDATED,
        'contribution_way' => ContributionWay::SEPA,
    ], User::factory()->create(['role' => $role]));

    Mail::fake();
    Livewire::test(ShowApplication::class, ['application' => $application])
        ->assertSee(__('dashboard.actions.complete'))
    ;

    Livewire::test(ApplicationComplete::class, ['application' => $application])
        ->assertSeeHtml(__('dashboard.actions.complete'))
        ->call('complete')
        ->assertHasErrors()
        ->set('application.first_withdrawal_date', Carbon\Carbon::now()->addMonth()->format('Y-m-d'))
        ->call('complete')
        ->assertHasNoErrors()
    ;

    expect($application->refresh()->status)->toEqual(ApplicationStatus::COMPLETED);

    Mail::assertNothingSent();
    Storage::disk('nextcloud')
        ->assertExists("{$application->firstname} {$application->name} {$application->getShortenedId()}.pdf");
})->with([Role::TREASURY, Role::ADMIN]);

test('can mark as subscribed to mailing lists', function (): void {
    $application = createApplication(['status' => ApplicationStatus::COMPLETED]);

    $this->actingAs(User::factory()->create(['role' => Role::BOARD]));

    Mail::fake();
    Livewire::test(ShowApplication::class, ['application' => $application])
        ->assertSee(__('dashboard.actions.done'))
        ->call('done')
        ->assertDispatched('move-application', applicationId: $application->id)
        ->assertHasNoErrors()
    ;

    expect($application->refresh()->status)->toEqual(ApplicationStatus::READY);

    Mail::assertQueued(WelcomeNotification::class);
});

test('can mark as welcomed', function (): void {
    $application = createApplication(['status' => ApplicationStatus::READY]);

    $this->actingAs(User::factory()->create(['role' => Role::BOARD]));

    Livewire::test(ShowApplication::class, ['application' => $application])
        ->assertSee(__('dashboard.actions.done'))
        ->call('done')
        ->assertDispatched('move-application', applicationId: $application->id)
        ->assertHasNoErrors()
    ;

    expect($application->refresh()->status)->toEqual(ApplicationStatus::WELCOMED);
});

test("can't complete application with the wrong role", function (Role $role): void {
    $application = createApplication(['status' => ApplicationStatus::NEW], User::factory()->create(['role' => $role]));

    Mail::fake();
    Livewire::test(ApplicationComplete::class, ['application' => $application])
        ->set('application.first_withdrawal_date', Carbon\Carbon::now()->addMonth()->format('Y-m-d'))
        ->call('complete')
        ->assertForbidden()
    ;

    expect($application->refresh()->status)->toEqual(ApplicationStatus::NEW);

    Mail::assertNothingSent();
})->with([Role::BOARD, Role::INACTIVE]);

test('show status of completed application', function (): void {
    $application = createApplication(['status' => ApplicationStatus::COMPLETED], User::factory()->create(['role' => Role::ADMIN]));

    Livewire::test(ShowApplication::class, ['application' => $application])
        ->assertSee(__('dashboard.status.'.ApplicationStatus::COMPLETED->value, ['timeDiff' => $application->updated_at->diffForHumans()]))
    ;
});

/**
 * @return array<string, \App\Models\Application>
 */
function getApplications(): array
{
    return [
        ApplicationStatus::NEW->name => createApplication(['status' => ApplicationStatus::NEW]),
        ApplicationStatus::COMPLETED->name => createApplication(['status' => ApplicationStatus::COMPLETED]),
        ApplicationStatus::REJECTED->name => createApplication(['status' => ApplicationStatus::REJECTED]),
    ];
}

test('show completed and rejected applications', function (Role $role): void {
    $applications = getApplications();

    $this->actingAs(User::factory()->create(['role' => $role]));

    Livewire::test(ShowApplications::class, ['status' => [ApplicationStatus::REJECTED, ApplicationStatus::COMPLETED]])
        ->assertSeeText($applications[ApplicationStatus::COMPLETED->name]->address)
        ->assertSeeText($applications[ApplicationStatus::REJECTED->name]->address)
        ->assertDontSeeText($applications[ApplicationStatus::NEW->name]->address)
    ;
})->with([Role::BOARD, Role::TREASURY, Role::ADMIN]);

test('show new applications', function (Role $role): void {
    $applications = getApplications();

    $this->actingAs(User::factory()->create(['role' => $role]));

    Livewire::test(ShowApplications::class, ['status' => [ApplicationStatus::NEW->value]])
        ->assertSeeText($applications[ApplicationStatus::NEW->name]->address)
        ->assertDontSeeText($applications[ApplicationStatus::COMPLETED->name]->address)
        ->assertDontSeeText($applications[ApplicationStatus::REJECTED->name]->address)
    ;
})->with([Role::BOARD, Role::TREASURY, Role::ADMIN]);

function createApplication(array $data = [], User $login = null): \App\Models\Application
{
    Auth::login($login ?? User::factory()->create(['role' => Role::ADMIN]));
    $application = ApplicationFactory::new()->create($data);
    if ($login === null) {
        Auth::logout();
    }

    return $application;
}
