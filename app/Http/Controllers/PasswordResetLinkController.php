<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\RateLimiter;
use Laravel\Fortify\Contracts\FailedPasswordResetLinkRequestResponse;
use Laravel\Fortify\Contracts\SuccessfulPasswordResetLinkRequestResponse;
use Laravel\Fortify\Fortify;
use Laravel\Fortify\Http\Controllers\PasswordResetLinkController as FortifyController;

final class PasswordResetLinkController extends FortifyController
{
    private const RATE_LIMITER_KEY = 'reset-password';

    /**
     * Overwrite the Fortify method to avoid exposing if an account exists.
     */
    public function store(Request $request): Responsable
    {
        // Random sleep time to reduce time-based enumeration possibility
        usleep(random_int(400000, 1000000));

        if (RateLimiter::tooManyAttempts(self::RATE_LIMITER_KEY, 1)) {
            Log::warning('Reset password request blocked by the rate limiter', $request->only(Fortify::email()));

            return app(FailedPasswordResetLinkRequestResponse::class, ['status' => 'passwords.throttled']);
        }
        RateLimiter::hit(self::RATE_LIMITER_KEY);

        // Form validation
        $request->validate([Fortify::email() => 'required|email']);

        // Send the password reset link
        $this->broker()->sendResetLink(
            $request->only(Fortify::email())
        );

        // Always return a success
        return app(SuccessfulPasswordResetLinkRequestResponse::class, ['status' => Password::RESET_LINK_SENT]);
    }
}
