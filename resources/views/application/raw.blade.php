<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>{{ $application->firstname }} {{ $application->name }}</title>
    <style>
        h1, h3 {
            font-size: large;
        }
        dd {
            margin-bottom: .5em;
        }
    </style>
</head>
<body>
    <h1>
        {{ $application->firstname }} {{ $application->name }}
    </h1>
    <hr>
    <livewire:application-overview :application="$application" :showRestrictedInformation="true" />
    <hr>
    <p>
        Adhésion reçue le {{ $carbonFactory->make($application->created_at)->isoFormat('llll') }}
        <br>
        Document généré le {{ $carbonFactory->make(now())->isoFormat('llll') }}
        <br>
        ID de demande d'adhésion : <code style="font-size: xx-small">{{ $application->id }}</code>
    </p>
</body>
</html>
