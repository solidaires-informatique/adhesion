<?php

namespace Database\Factories;

use App\Models\Option;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

final class OptionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Option::class;

    public function definition()
    {
        return [
            'key' => $this->faker->uuid(),
            'value' => $this->faker->uuid(),
            'user_id' => null,
        ];
    }
}
