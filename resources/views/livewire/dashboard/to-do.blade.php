<dl class="grid grid-cols-4 gap-6 m-4">
    @foreach($counts as $key => $count)
        <div>
            <label class="block text-sm mb-2">
                S'abonner
                <input type="checkbox" value="{{$key}}" wire:model.live="notifications">
            </label>

            <a href="{{ route('dashboard.index', ['filter' => $key]) }}">
                <div class="p-4 shadow bg-white rounded flex justify-between @if($filter === $key) border-si-blue-dark border-2 @endif">
                    <dt class="text-lg">{{ __("dashboard.todo.$key") }}</dt>
                    <dd>{{ $count }}</dd>
                </div>
            </a>
        </div>
    @endforeach
</dl>
