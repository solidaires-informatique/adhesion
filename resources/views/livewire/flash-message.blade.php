<div class="mx-auto max-w-xl border m-4 rounded" role="alert" aria-live="assertive" aria-atomic="true" data-mdb-autohide="false">
    <p class="bg-white px-4 py-5 sm:px-6 rounded">
        {{ $message }}
    </p>
</div>
