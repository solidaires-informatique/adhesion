<?php

declare(strict_types=1);

namespace App\Livewire;

/**
 * @source https://github.com/livewire/livewire/issues/823#issuecomment-821074805
 */
trait ConvertEmptyStringsToNull
{
    /**
     * @var string[]
     */
    protected array $convertEmptyStringsExcept = [];

    public function updatedConvertEmptyStringsToNull(string $name, mixed $value): void
    {
        if (!\is_string($value) || \in_array($name, $this->convertEmptyStringsExcept, true)) {
            return;
        }

        $value = trim($value);
        $value = $value === '' ? null : $value;

        data_set($this, $name, $value);
    }
}
