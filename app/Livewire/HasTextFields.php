<?php

declare(strict_types=1);

namespace App\Livewire;

use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Validation\Rules\RequiredIf;

trait HasTextFields
{
    /**
     * @param array<int, string|ValidationRule|RequiredIf> $rules
     *
     * @return array<int, string|ValidationRule|RequiredIf>
     */
    public static function getTextValidators(bool $required, array $rules = []): array
    {
        return [$required ? 'required' : 'nullable', 'string', 'max:255', ...$rules];
    }
}
