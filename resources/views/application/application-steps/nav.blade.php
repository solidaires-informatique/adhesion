<nav class="my-4" aria-label="étapes du formulaire">
    <ul class="sm:grid gap-10 lg:grid-cols-4 sm:grid-cols-2">
        <li class="flex items-center justify-between h-full">
            <p class="text-lg sm:text-2xl @if($step === 1) font-bold @endif" aria-current="{{ $step === 1 ? 'step' : 'false' }}">
                <span class="sm:hidden">1)</span> Informations personnelles
            </p>
            @include('application.application-steps.arrow')
        </li>
        <li class="flex items-center justify-between h-full">
            <p class="text-lg sm:text-2xl @if($step === 2) font-bold @endif" aria-current="{{ $step === 2 ? 'step' : 'false' }}">
                <span class="sm:hidden">2)</span> Situation professionnelle
            </p>
            @include('application.application-steps.arrow')
        </li>
        <li class="flex items-center justify-between h-full">
            <p class="text-lg sm:text-2xl @if($step === 3) font-bold @endif" aria-current="{{ $step === 3 ? 'step' : 'false' }}">
                <span class="sm:hidden">3)</span> Cotisation
            </p>
            @include('application.application-steps.arrow')
        </li>
        <li class="flex items-center justify-between h-full">
            <p class="text-lg sm:text-2xl @if($step === 4) font-bold @endif" aria-current="{{ $step === 4 ? 'step' : 'false' }}">
                <span class="sm:hidden">4)</span> Finalisation
            </p>
        </li>
    </ul>
</nav>
