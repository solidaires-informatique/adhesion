<?php

use App\Models\User;
use App\Rules\Iban;
use Illuminate\Support\Facades\Hash;
use Laravel\Jetstream\Http\Livewire\UpdatePasswordForm;
use Livewire\Livewire;

test('validate french IBAN without spaces', function () {
    $iban = fake()->iban('fr');
    $rule = new Iban();
    $fail = static fn () => expect(false)->toBeTrue($iban);

    $rule->validate(
        attribute: 'iban',
        value: $iban,
        fail: $fail,
    );

    expect(true)->toBeTrue();
})->repeat(50);

test('validate french IBAN with spaces', function (int $chunkLength) {
    $iban = chunk_split(fake()->iban('fr'), $chunkLength, ' ');
    $rule = new Iban();
    $fail = static fn () => expect(false)->toBeTrue($iban);

    $rule->validate(
        attribute: 'iban',
        value: $iban,
        fail: $fail,
    );

    expect(true)->toBeTrue();
})->with([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

