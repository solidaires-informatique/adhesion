<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Enums\ApplicationStatus;
use App\Models\Application;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

final class PurgeApplicationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'applications:purge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drop old applications from the database.';

    public function handle(): int
    {
        $count = (string) Application::whereIn('status', [
            ApplicationStatus::REJECTED,
            ApplicationStatus::WELCOMED,
        ])->where('updated_at', '<', date('Y-m-d', strtotime('-1 month', time())))->delete();

        $message = "{$count} deleted applications";
        $this->info($message);
        Log::info($message, [$this->signature]);

        return self::SUCCESS;
    }
}
