<h1>
    À faire
</h1>

@foreach($todo as $status => $names)
    <h2>
        <a href="{{ route('dashboard.index', ['filter' => $status]) }}">
            {{ __("dashboard.todo.{$status}") }}
        </a>
    </h2>
    <blockquote>{{ $names }}</blockquote>
@endforeach

<footer>
    <p>
        Vous recevez ce résumé parce que vous vous êtes inscrit à l'une des tâches ci-dessus.
    </p>
</footer>
