<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Actions\Fortify\CreateNewUser;
use App\Enums\Role;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Symfony\Component\Console\Command\Command as CommandAlias;

final class AdminCreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create
    {pseudo}
    {--cd : Continuous deployment mode, ignore the used pseudo error}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an admin';

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $pseudo = $this->argument('pseudo');
        if (!\is_string($pseudo)) {
            $this->info('Invalid pseudo.');

            return CommandAlias::FAILURE;
        }

        $password = Str::random(12);

        $createNewUser = new CreateNewUser();

        try {
            $createNewUser->create([
                'pseudo' => $pseudo,
                'password' => $password,
                'password_confirmation' => $password,
                'role' => Role::ADMIN,
            ]);
        } catch (ValidationException $exception) {
            if ($exception->getMessage() !== __('validation.unique', ['attribute' => 'pseudo'])) {
                throw $exception;
            }

            $message = "{$pseudo} is already used";
            if ($this->option('cd')) {
                $this->warn($message);

                return CommandAlias::SUCCESS;
            }

            $this->error($message);

            return CommandAlias::FAILURE;
        }

        $profileUrl = route('profile.show');

        $this->info("Le compte d'administration a été créée.");
        $this->info("Pseudo : {$pseudo}");
        $this->info("Mot de passe : {$password} (à changer sur {$profileUrl})");

        return CommandAlias::SUCCESS;
    }
}
