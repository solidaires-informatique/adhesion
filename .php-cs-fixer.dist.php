<?php

use PhpCsFixer\Config;
use PhpCsFixer\Finder;

$rules = [
    '@PHP83Migration' => true,
    '@PHP80Migration:risky' => true,
    '@Symfony' => true,
    '@Symfony:risky' => true,

    // Overwrite ruleset
    // Enforce non-Yoda style.
    'yoda_style' => ['equal' => false, 'identical' => false, 'less_and_greater' => false],

    // Additional rules
    'no_superfluous_elseif' => true,
    'no_useless_else' => true,
    'return_assignment' => true,
    'multiline_comment_opening_closing' => true,
    'explicit_string_variable' => true,
    'explicit_indirect_variable' => true,
    'combine_consecutive_issets' => true,
    'combine_consecutive_unsets' => true,
];


$finder = Finder::create()
    ->in([
        __DIR__ . '/app',
    ])
    ->name('*.php')
    ->notName('*.blade.php')
    ->ignoreDotFiles(true)
    ->ignoreVCS(true);

return (new Config)
    ->setParallelConfig(PhpCsFixer\Runner\Parallel\ParallelConfigFactory::detect())
    ->setFinder($finder)
    ->setRules($rules)
    ->setRiskyAllowed(true)
    ->setUsingCache(true);
