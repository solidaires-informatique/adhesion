<x-mail::message>
Bonjour {{ $application->firstname }},

Ton adhésion vient d’être prise en compte.

Tu as été ajouté·e à une ou plusieurs listes :

- diffusion@listes.solidairesinformatique.org : C'est la liste de tous les adhérent·es du syndicat. Cette liste est modérée : un message envoyé doit d'abord être approuvé
- La liste de diffusion de ton département/territoire. N’hésite pas à y envoyer un mail de présentation
- Pour ta section d’entreprise, je mets en copie les représentant·es syndicaux éventuel·les

@if($application->contribution_way === \App\Enums\ContributionWay::CHECK)
<x-mail::panel>
Pense à envoyer ta cotisation par chèque à l'adresse suivante :

Solidaires Informatique, 31 rue de la Grange aux Belles 75010 Paris
</x-mail::panel>
@elseif($application->contribution_way === \App\Enums\ContributionWay::TRANSFER)
<x-mail::panel>
Voici notre RIB pour envoyer ta cotisation :

IBAN : {{ $iban }}

BIC : {{ $bic }}

Si possible programme un virement automatique mensuel avec comme motif :
Cotisation {{ mb_strtoupper($application->firstname) }} {{ mb_strtoupper($application->name) }}
</x-mail::panel>
@elseif($application->contribution_way === \App\Enums\ContributionWay::SEPA)
Le premier prélèvement aura lieu le {{ (new DateTime($application->first_withdrawal_date))->format('d/m/y') }}.

Sache que, si tu ne déclares pas tes frais réels, tu peux déduire 66% de tes cotisations de tes impôts (sous forme d’un crédit d’impôt).
Fin mars de l'année suivante, une attestation par mail est transmise qui récapitule et justifie des sommes déclarées.
@endif

<x-mail::panel>
Pour t’accueillir et te donner les premières informations sur notre fonctionnement nous organisons des visios d’accueil.
Tu peux t’inscrire à une prochaine date ici : [Inscription aux visio d’accueil]({{ $welcome_session_link }})
Nous ajoutons régulièrement de nouvelles dates, n’hésite pas retourner sur le lien si tu ne peux pas tout de suite.

Mais si tu souhaites un contact plus rapide n’hésite pas à contacter la commission accueil accueil@solidairesinformatique.org.
Cette commission pourra t’orienter vers les instances du syndicat qui pourront répondre à ta situation.

Pense à consulter notre livret d’accueil : [SI_Accueil.pdf](https://cloud.solidairesinformatique.org/s/QPG9KW83xqALdi4)
</x-mail::panel>

Encore une fois, sois bienvenu·e, n’hésite pas à poser des questions, il y aura toujours quelqu'un pour te répondre.

Pour une question sur le droit du travail ou sur la relation avec ton employeur, par exemple si tu as un problème en cours, contacte [entraide@listes.solidairesinformatique.org](mailto:entraide@listes.solidairesinformatique.org).

Solidairement, le bureau de Solidaires Informatique ([contact@solidairesinformatique.org](mailto:contact@solidairesinformatique.org))
</x-mail::message>
