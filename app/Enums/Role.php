<?php

declare(strict_types=1);

namespace App\Enums;

enum Role: int
{
    case INACTIVE = 99;
    case ADMIN = 1;
    case TREASURY = 2;
    case BOARD = 3;
}
