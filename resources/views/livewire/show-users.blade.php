<div class="align-middle inline-block min-w-full shadow overflow-hidden bg-white shadow-dashboard m-3 p-3 rounded rounded-br-lg">

    <div class="px-4 py-5 sm:px-6">
        <a class="form__submit form__submit_success" href="{{ route('admin.users.create') }}">{{ __('dashboard.users.create') }}</a>
    </div>

    <table class="min-w-full">
        <thead>
        <tr>
            <th class="p-4 text-left leading-4 text-si-blue-dark sm:table-cell hidden">ID</th>
            <th class="p-4 text-left text-sm leading-4 text-si-blue-dark">Pseudo</th>
            <th class="p-4 text-left text-sm leading-4 text-si-blue-dark">Role</th>
            <th class="p-4 text-left text-sm leading-4 text-si-blue-dark sm:table-cell hidden">Email</th>
            <th class="p-4 text-left text-sm leading-4 text-si-blue-dark lg:table-cell hidden">2FA</th>
            <th class="p-4 text-left text-sm leading-4 text-si-blue-dark lg:table-cell hidden">Date de création</th>
            <th class="p-4"></th>
        </tr>
        </thead>
        <tbody class="bg-white">
        @foreach ($users as $user)
            <tr>
                <td class="p-4 whitespace-no-wrap sm:table-cell hidden text-sm leading-5 text-gray-800">
                    #{{ $user->id }}
                </td>
                <td class="p-4 whitespace-no-wrap">
                    <div class="text-sm leading-5">{{ $user->pseudo }}</div>
                </td>
                <td class="p-4 whitespace-no-wrap text-sm leading-5">{{ __('dashboard.role.' . $user->role->value) }}</td>
                <td class="p-4 whitespace-no-wrap text-sm leading-5 sm:table-cell hidden @if($user->email === null) italic @endif">{{ $user->email ?: 'Non renseigné' }}</td>
                <td class="p-4 whitespace-no-wrap text-sm leading-5 lg:table-cell hidden">{{ $user->hasEnabledTwoFactorAuthentication() ? 'Oui' : 'Non' }}</td>
                <td class="p-4 whitespace-no-wrap text-sm leading-5 lg:table-cell hidden">{{ $user->created_at->format('d/m/Y') }}</td>
                <td class="p-4 whitespace-no-wrap text-sm leading-5">
                    <a href="{{ route('admin.users.edit', ['user' => $user->id]) }}" class="px-5 py-2lue-500 border text-si-blue-dark rounded transition duration-300 hover:bg-si-blue-dark hover:text-white focus:outline-none">Modifier</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
