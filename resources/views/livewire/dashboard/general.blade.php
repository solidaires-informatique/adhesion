<main>
    @if (!$this->hasEnabledTwoFactorAuthentication)
    <section class="mt-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white px-4 py-5 sm:px-6 rounded">
                <h2 class="text-xl font-bold">Vous n'avez pas activé l'authentification à deux facteurs.</h2>

                <p>
                    Lorsque l'authentification à deux facteurs est activée, un jeton sécurisé et aléatoire vous est demandé lors de l'authentification.
                    Vous pouvez récupérer ce jeton à partir d'une application mobile dédiée ou d'un gestionnaire de mots de passe.
                </p>

                <p>
                    Il s'agit d'une fonctionnalité de sécurité importante.
                    Pour l'activer, rendez-vous sur <a class="link" href="{{ route('profile.show')}}">votre profil</a>.
                </p>
            </div>
        </div>
    </section>
    @endif

    @if ($this->shouldUpdateWelcomeSessionLink)
    <section class="mt-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white px-4 py-5 sm:px-6 rounded">
                <h2 class="text-xl font-bold">Il faut changer le {{ mb_strtolower(__('dashboard.settings.'.\App\Enums\OptionEnum::WELCOME_SESSION_LINK->value)) }}</h2>

                <p>
                    Le {{ mb_strtolower(__('dashboard.settings.'.\App\Enums\OptionEnum::WELCOME_SESSION_LINK->value)) }} n'a pas été changé depuis au moins 6 mois.
                </p>
            </div>
        </div>
    </section>
    @endif
</main>
