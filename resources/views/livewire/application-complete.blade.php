<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Finaliser l'adhésion
        </h2>
        <p>
            La finalisation de l'adhésion entraîne l'ajout de la personne dans la base d'adhérent·es et notifie le bureau ainsi que le
            groupe d'accueil.
        </p>
    </x-slot>

    <div class="form__section">
        <p class="form__section__instruction">
            {{ $application->firstname }} {{ $application->name }}
            <span title="{{ $application->id }}" class="font-mono text-xs">{{ $application->getShortenedId() }}</span>
        </p>

        @if($application->contribution_way === \App\Enums\ContributionWay::SEPA)
            <p class="my-2">
                Ces informations seront envoyées dans le mail de bienvenue.
            </p>

            <div class="form__input-group">
                <div class="form__input-group__element">
                    <label class="form__input-group__element__label" for="first_withdrawal_date">
                        Date du premier prélèvement
                    </label>
                    <input
                        wire:model.blur="application.first_withdrawal_date"
                        id="first_withdrawal_date"
                        type="date"
                        required
                        min="{{\Carbon\Carbon::now()->format('Y-m-d')}}"
                        max="{{\Carbon\Carbon::now()->addMonths(2)->format('Y-m-d')}}"
                        class="form__input-group__element__input @error('application.first_withdrawal_date') errored @enderror"
                    >
                    @error('application.first_withdrawal_date') <p class="form__input-group__element__error">{{ $message }}</p> @enderror

                </div>
            </div>
        @endif

        <p class="my-2">
            L'ajout des données dans le CRM n'est pas encore automatisé, il faut le faire manuellement à cette étape.
        </p>

        <input
            wire:loading.attr="disabled"
            wire:click.prevent="complete"
            type="submit"
            class="form__submit @error('submit') form__submit_error @enderror"
            value="{{ __('dashboard.actions.complete') }}"
        >

        <p wire:loading class="inline">
            Sauvegarde en cours…
        </p>

        <p class="my-2">
            Cette étape archive automatiquement le bulletin d'adhésion
            @if($application->contribution_way === \App\Enums\ContributionWay::SEPA) et le mandat SEPA @endif
            en PDF sur le cloud.
        </p>

        <livewire:application-overview :application="$application" :showRestrictedInformation="true" />
    </div>
</div>
