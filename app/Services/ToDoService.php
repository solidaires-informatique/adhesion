<?php

declare(strict_types=1);

namespace App\Services;

use App\Enums\ApplicationStatus;
use App\Models\Application;
use Illuminate\Database\Eloquent\Collection;

final class ToDoService
{
    /** @var ApplicationStatus[] */
    private const array STATUSES = [
        ApplicationStatus::NEW,
        ApplicationStatus::VALIDATED,
        ApplicationStatus::COMPLETED,
        ApplicationStatus::READY,
    ];

    /**
     * @return array<value-of<ApplicationStatus>, string>
     */
    public function getApplicantNames(): array
    {
        /** @var array<value-of<ApplicationStatus>, Collection<int, Application>> $todo */
        $todo = Application::query()
            ->select(['id', 'firstname', 'name', 'status'])
            ->whereIn('status', self::STATUSES)
            ->get()
            ->groupBy('status')
            ->all();

        $names = [];
        foreach ($todo as $status => $applications) {
            $names[$status] = $applications->map(static fn (Application $application) => $application->getTitle())->implode(', ');
        }

        return $names;
    }
}
