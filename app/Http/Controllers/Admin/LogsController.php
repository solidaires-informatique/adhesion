<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Jackiedo\LogReader\LogReader;

final class LogsController extends Controller
{
    public function __invoke(LogReader $reader): Factory|View|Application
    {
        $logs = $reader->orderBy('date', 'desc')->get();

        return view('admin.logs', ['logs' => $logs]);
    }
}
