<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('options', function (Blueprint $table) {
            $table->id();
            $table->string('key')->index();
            $table->foreignId('user_id')->nullable()->index();
            $table->string('value')->nullable();
            $table->timestamp('updated_at')->default('now');
        });
    }
};
