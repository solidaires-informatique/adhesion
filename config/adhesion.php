<?php

return [
    'sirene' => [
        'key' => env('SIRENE_API_KEY', ''),
    ],
];
