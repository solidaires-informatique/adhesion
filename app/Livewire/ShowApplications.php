<?php

declare(strict_types=1);

namespace App\Livewire;

use App\Enums\ApplicationStatus;
use App\Models\Application;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Livewire\Attributes\On;
use Livewire\Component;

final class ShowApplications extends Component
{
    use AuthorizesRequests;

    public string $title;
    public string $subtitle;

    /**
     * @var ApplicationStatus[]
     */
    public array $status;

    /**
     * @var Collection<int, Application>
     */
    public Collection $applications;

    /**
     * @throws AuthorizationException
     */
    public function mount(): void
    {
        $this->applications = Application::whereIn('status', $this->status)->orderBy('created_at')->get();
    }

    #[On('move-application')]
    public function removeApplication(string $applicationId): void
    {
        $this->applications = $this->applications->reject(
            static fn (Application $application) => $application->id === $applicationId
        );
    }
}
