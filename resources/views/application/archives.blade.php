<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Archives') }}
        </h2>
    </x-slot>

    <div class="mt-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="px-4 py-5 sm:px-6">
                <p>
                    Voici les demandes d'adhésion déjà traitées.
                    Ces données sont automatiquement supprimées après un mois.
                </p>
            </div>
        </div>
    </div>

    <div class="mt-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <livewire:show-applications :status="['rejected', 'welcomed']" />
        </div>
    </div>
</x-app-layout>
