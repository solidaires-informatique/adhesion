<?php

use App\Enums\ApplicationStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->uuid('id')->primary()->unique();

            $table->string('name');
            $table->string('firstname');
            $table->string('address');
            $table->string('postcode');
            $table->string('city');
            $table->string('email');
            $table->string('mobile');

            $table->string('company_name')->nullable();
            $table->string('company_siret')->nullable();
            $table->string('company_legal_address')->nullable();
            $table->string('company_work_address')->nullable();

            $table->integer('birth_year')->nullable();
            $table->string('gender')->nullable();

            $table->string('contribution_way');
            $table->float('contribution')->unsigned();

            $table->string('sepa_full_name')->nullable();
            $table->string('sepa_address')->nullable();
            $table->string('sepa_postcode')->nullable();
            $table->string('sepa_city')->nullable();
            $table->string('sepa_country')->nullable();
            $table->string('iban')->nullable();
            $table->string('bic')->nullable();
            $table->string('made_at')->nullable();
            $table->text('signature')->nullable();

            $table->boolean('mail_local')->default(false);

            $table->text('applicant_comment')->nullable();

            $table->string('status')->default(ApplicationStatus::NEW->value);

            $table->string('RUM')->nullable();
            $table->date('first_withdrawal_date')->nullable();

            $table->timestamps();
        });
    }
};
