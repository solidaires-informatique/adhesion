<?php

declare(strict_types=1);

namespace App\Livewire;

use Livewire\Attributes\Validate;
use Livewire\Component;
use Livewire\Features\SupportFileUploads\TemporaryUploadedFile;
use Livewire\WithFileUploads;

final class SignatureInput extends Component
{
    use WithFileUploads;

    #[Validate('image|mimes:png,jpg|max:200')]
    public ?TemporaryUploadedFile $signatureFile = null;

    public function updated(string $propertyName): void
    {
        if ($propertyName === 'signatureFile' && $this->signatureFile instanceof TemporaryUploadedFile) {
            $this->dispatch(
                'updateSignature',
                "data:{$this->signatureFile->getMimeType()};base64,".base64_encode((string) $this->signatureFile->get())
            );
        }
    }
}
