<?php

namespace Database\Seeders;

use App\Enums\ApplicationStatus;
use App\Enums\ContributionWay;
use App\Enums\OptionEnum;
use App\Models\Application;
use App\Models\Option;
use App\Services\FileService;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

final class ApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Storage::fake('nextcloud');

        Option::factory()->create([
            'value' => 'FAKEBIC',
            'key' => OptionEnum::UNION_BIC->value,
        ]);

        Option::factory()->create([
            'value' => fake()->iban(),
            'key' => OptionEnum::UNION_IBAN->value,
        ]);

        Option::factory()->create([
            'value' => 'https://example.org',
            'key' => OptionEnum::WELCOME_SESSION_LINK->value,
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::REJECTED
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::NEW,
            'contribution_way' => ContributionWay::SEPA,
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::NEW,
            'contribution_way' => ContributionWay::CHECK,
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::VALIDATED,
            'contribution_way' => ContributionWay::SEPA,
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::VALIDATED,
            'contribution_way' => ContributionWay::TRANSFER,
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::VALIDATED,
            'contribution_way' => ContributionWay::CHECK,
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::COMPLETED
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::READY
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::WELCOMED
        ]);
    }
}
