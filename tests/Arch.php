<?php

use App\Http\Controllers\Controller;

arch()->preset()->php();

arch()->preset()->security();

arch()->preset()->laravel();

arch()->expect('App')
    ->toUseStrictTypes()
    ->toUseStrictEquality()
    ->expect(['dd', 'dump', 'ray', 'die', 'var_dump', 'sleep'])->not->toBeUsed();

arch()
    ->expect('App')
    ->classes()
    ->toBeFinal()
    ->ignoring([Controller::class]);

arch()->expect('App\Services')->classes()->not->toHaveProtectedMethods();
arch()->expect('App\Http\Controllers')->classes()->not->toHaveProtectedMethods();
arch()->expect('App\Events')->classes()->not->toHaveProtectedMethods();
arch()->expect('App\Listeners')->classes()->not->toHaveProtectedMethods();
arch()->expect('App\Mail')->classes()->not->toHaveProtectedMethods();

