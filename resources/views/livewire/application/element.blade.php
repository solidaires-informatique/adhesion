<div x-on:click="navigator.clipboard.writeText('{{ trim(json_encode($content), '"') }}')">
    <dt class="text-gray-600">{{ $title }}</dt>
    <dd>{{ $content }}</dd>
</div>
