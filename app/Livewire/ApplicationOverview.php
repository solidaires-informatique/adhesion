<?php

declare(strict_types=1);

namespace App\Livewire;

use App\Models\Application;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;
use Livewire\Component;

final class ApplicationOverview extends Component
{
    public Application $application;
    public bool $showRestrictedInformation;

    public function mount(): void
    {
        $this->tryToFormatPhone();
    }

    private function tryToFormatPhone(): void
    {
        $phoneNumberUtil = PhoneNumberUtil::getInstance();

        try {
            $number = $phoneNumberUtil->parse($this->application->mobile, 'FR');
            if (
                $phoneNumberUtil->isPossibleNumber($number)
                && $phoneNumberUtil->isValidNumberForRegion($number, 'FR')
            ) {
                $this->application->mobile = $phoneNumberUtil->format($number, \libphonenumber\PhoneNumberFormat::NATIONAL);
            }
        } catch (NumberParseException) {
        }
    }
}
