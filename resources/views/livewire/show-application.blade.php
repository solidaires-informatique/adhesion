<div class="overflow-hidden bg-white shadow border-2 mb-6 sm:rounded-lg">
    <div class="border-b mb-2 flex justify-between px-4 py-5 sm:px-6 align-middle">
        <div class="flex">
            <h3 class="m-auto text-lg leading-6 text-gray-900">
                {{ $application->firstname }} {{ $application->name }}
                <span title="{{ $application->id }}" class="font-mono text-xs">{{ $application->getShortenedId() }}</span>
            </h3>
        </div>
        <div class="flex items-baseline flex-wrap">
            <p class="mx-1">
                    {{ ucfirst(__("dashboard.status.new", ['timeDiff' => $application->created_at->diffForHumans()])) }}
                @if($application->status->value !== 'new')
                    et {{ __("dashboard.status.{$application->status->value}", ['timeDiff' => $application->updated_at->diffForHumans()]) }}.
                @endif

            </p>

            <div class="flex flex-wrap">
                @if($canDone)
                    <form wire:submit="done" class="mx-1">
                        <input class="form__submit w-full my-1 md:w-auto" type="submit" value="{{$doneName}}" />
                    </form>
                @endif
                @if($canUndo)
                    <form wire:submit="undo" class="mx-1">
                        <button class="form__submit w-full my-1 md:w-auto" wire:click.prevent="$set('confirmUndo', true)">Remettre à "{{ __('dashboard.todo.'.$application->status->prev()->value)}}"</button>
                    </form>
                @endif
                @if($canReject)
                    <form wire:submit="reject" class="mx-1">
                        <input class="form__submit form__submit_error w-full my-1 md:w-auto" type="submit" value="{{ __('dashboard.actions.reject') }}" />
                    </form>
                @endif
            </div>
        </div>
    </div>

    <livewire:application-overview :application="$application" :showRestrictedInformation="true" />

    @if ($canUndo)
    <x-confirmation-modal wire:model.live="confirmUndo">
        <x-slot name="title">
        Revenir en arrière ({{ $application->firstname }} {{ $application->name }})
        </x-slot>

        <x-slot name="content">
        Êtes-vous sûr de vouloir remettre cette adhésion dans "{{ __('dashboard.todo.'.$application->status->prev()->value)}}" ?
        </x-slot>

        <x-slot name="footer">
        <x-secondary-button wire:click="$toggle('confirmUndo')" wire:loading.attr="disabled">
            Non
        </x-secondary-button>

        <x-button class="ml-2" wire:click="undo" wire:loading.attr="disabled">
            Oui
        </x-button>
        </x-slot>
    </x-confirmation-modal>
    @endif
</div>

