<?php

declare(strict_types=1);

namespace Tests\Feature\Livewire;

use Tests\TestCase;
use Livewire\Livewire;
use App\Models\Application;
use App\Enums\ContributionWay;
use App\Livewire\ApplicationForm;
use Illuminate\Support\Facades\Mail;
use PHPUnit\Framework\Attributes\Test;
use Database\Factories\ApplicationFactory;
use App\Mail\ApplicationReceivedConfirmation;
use Livewire\Features\SupportTesting\Testable;
use Illuminate\Foundation\Testing\RefreshDatabase;

final class ApplicationFormTest extends TestCase
{
    use RefreshDatabase;

    #[Test]
    function can_submit_application(): void
    {
        Mail::fake();
        $factory = ApplicationFactory::new();
        $application = $factory->state($factory->withCompanyState())->make();
        Livewire::test(ApplicationForm::class, ['application' => $application])
            ->set('wage', 1312)
            ->set('terms', true)
            ->call('submit')
            ->assertHasNoErrors()
            ->assertOk();

        $this->assertTrue(Application::query()
            ->where('name', $application->name)
            ->where('firstname', $application->firstname)
            ->exists());

        Mail::assertQueued(ApplicationReceivedConfirmation::class);
    }

    #[Test]
    function cant_go_to_next_step_with_error(): void
    {
        $test = $this->firstStep()
            ->set('application.mobile')
            ->call('next');
        $test->assertHasErrors('application.mobile');
        $this->assertStep1($test);
    }

    #[Test]
    function can_go_to_next_step(): void
    {
        $test = $this->firstStep()
            ->call('next')
            ->assertHasNoErrors()
            ->assertOk();
        $this->assertStep2($test);
    }

    #[Test]
    function can_go_back_in_application(): void
    {
        $test = $this->firstStep()->call('next')->call('back');
        $this->assertStep1($test);
    }

    #[Test]
    function cant_ignore_company_step(): void
    {
        $test = $this->firstStep()
            ->set('step', 2)
            ->call('next')
            ->assertHasErrors(['application.company_siret', 'application.company_name']);

        $test->set('noSiret', true)
            ->call('next')
            ->assertHasErrors(['application.company_name']);

        $test->set('employee', false)
            ->call('next')
            ->assertHasNoErrors();

        $this->assertStep3($test);
    }

    #[Test]
    function can_fill_sepa(): void
    {
        Mail::fake();
        $test = $this->secondStep()->set('step', 3);
        $test->assertSee('Mandat de prélèvement SEPA');

        $test->set('wage', '2000');
        $test->assertSet('application.contribution', 13);

        $test->set('application.sepa_full_name', fake()->name());
        $test->set('application.sepa_address', fake()->address());
        $test->set('application.sepa_postcode', fake()->postcode());
        $test->set('application.sepa_city', fake()->city());
        $test->set('application.sepa_country', 'France');
        $test->set('application.iban', 'GB82 WEST 1234 5698 7654 32');
        $bic = fake()->swiftBicNumber();
        $test->set('application.bic', $bic);
        $test->set('application.made_at', fake()->city());
        $test->set('application.signature', 'data:image/png;base64,fakedatatofillthebase64SwAAAABkCAMAAAAL3/3=');

        $test->assertHasNoErrors()->assertOk();
        $test->call('next');
        $test->assertHasNoErrors()->assertOk();

        $test->assertSet('step', 4);
        $test->assertSee('Récapitulatif de vos informations');
        $test->assertSee($bic);

        $test->set('terms', true)
            ->call('submit')
            ->assertHasNoErrors()
            ->assertOk();

        Mail::assertQueued(ApplicationReceivedConfirmation::class);

        $this->assertTrue(Application::query()
            ->where('bic', $bic)
            ->where('contribution', 13)
            ->exists());
    }

    #[Test]
    function cant_fill_sepa_data_on_others_contribution_way(): void
    {
        Mail::fake();

        $test = $this->secondStep()->set('step', 3);
        $test->assertSee('Mandat de prélèvement SEPA');

        $test->set('application.contribution_way', ContributionWay::TRANSFER);
        $test->assertDontSee('Mandat de prélèvement SEPA');

        $test->set('application.contribution_way', ContributionWay::CHECK);
        $test->assertDontSee('Mandat de prélèvement SEPA');

        $test->set('application.contribution_way', ContributionWay::SEPA);
        $test->assertSee('Mandat de prélèvement SEPA');

        $test->set('application.contribution_way', ContributionWay::TRANSFER);
        $test->assertDontSee('Mandat de prélèvement SEPA');

        $test->set('minimalContribution', true);
        $test->assertSet('application.contribution', 1);

        // To verify if after filling information and switching to another contribution way it's not saved.
        $bic = fake()->swiftBicNumber();
        $test->set('application.bic', $bic);

        $test->assertHasNoErrors()->assertOk();
        $test->call('next');
        $test->assertHasNoErrors()->assertOk();

        $test->assertSet('step', 4);
        $test->assertSee('Récapitulatif de vos informations');
        $test->assertDontSee('Mandat SEPA');
        $test->assertDontSee($bic);

        $test->set('terms', true)
            ->call('submit')
            ->assertHasNoErrors()
            ->assertOk();

        Mail::assertQueued(ApplicationReceivedConfirmation::class);

        $this->assertFalse(Application::query()
            ->where('bic', $bic)
            ->exists());

        $this->assertTrue(Application::query()
            ->where('contribution_way', ContributionWay::TRANSFER->value)
            ->where('contribution', 1)
            ->exists());
    }

    private function firstStep(): Testable
    {
        return Livewire::test(ApplicationForm::class, ['application' => new Application()])
            ->set('application.name', fake()->name())
            ->set('application.firstname', fake()->name())
            ->set('application.address', fake()->address())
            ->set('application.postcode', fake()->postcode())
            ->set('application.city', fake()->city())
            ->set('application.email', fake()->safeEmail())
            ->set('application.mobile', fake()->phoneNumber());
    }

    private function secondStep(): Testable
    {
        return $this->firstStep()->set('step', 2)
            ->set('application.company_siret', 12345678901234)
            ->set('application.company_name', 'Super corp');
    }


    private function assertStep1(Testable $test): void
    {
        $test->assertSet('step', 1);
        $test->assertSee('Année de naissance');
        $test->assertDontSee("J'ai un employeur");
    }

    private function assertStep2(Testable $test): void
    {
        $test->assertSet('step', 2);
        $test->assertDontSee('Année de naissance');
        $test->assertSee("J'ai un employeur");
    }

    private function assertStep3(Testable $test): void
    {
        $test->assertSet('step', 3);
        $test->assertDontSee("J'ai un employeur");
        $test->assertSee('Taux de cotisation');
    }
}
