<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use SolidairesInformatique\BadParameter;
use SolidairesInformatique\ClientException;
use SolidairesInformatique\Etablissement;
use SolidairesInformatique\Sirene;

final readonly class SireneAPI
{
    private const string SIRENE_CACHE_ETABLISSEMENT = 'sirene-tablissement-';
    private Sirene $sirene;

    public function __construct()
    {
        $this->sirene = new Sirene(apiKey: config('adhesion.sirene.key'));
    }

    /**
     * @throws ClientException
     * @throws BadParameter
     */
    public function getWorkplace(string $siret): Etablissement
    {
        $etablissement = Cache::get(self::SIRENE_CACHE_ETABLISSEMENT.$siret);
        if ($etablissement instanceof Etablissement) {
            return $etablissement;
        }

        $etablissement = $this->sirene->siret($siret);
        Cache::set(self::SIRENE_CACHE_ETABLISSEMENT.$siret, $etablissement, 86400);

        return $etablissement;
    }

    /**
     * @throws ClientException
     * @throws BadParameter
     */
    public function getHeadOffice(Etablissement $workplace): Etablissement
    {
        if ($workplace->etablissementSiege) {
            return $workplace;
        }

        return $this->getWorkplace($workplace->getHeadOfficeSiret());
    }
}
