<x-form-section submit="updateNotifications">
    <x-slot name="title">
        {{ __('Notifications') }}
    </x-slot>

    <x-slot name="description">
    </x-slot>

    <x-slot name="form">
        <div class="col-span-6 sm:col-span-4">
            <x-label for="new"
                     value="Recevoir un mail quand une nouvelle adhésion est reçue."/>
            <x-input id="new" type="checkbox"
                     wire:model="new"/>
            <x-input-error for="new" class="mt-2"/>
        </div>

        <div class="col-span-6 sm:col-span-4">
            <x-label for="validated"
                     value="Recevoir un mail quand une adhésion est validée"/>
            <x-input id="validated" type="checkbox"
                     wire:model="validated"/>
            <x-input-error for="validated" class="mt-2"/>
        </div>

        <div class="col-span-6 sm:col-span-4">
            <x-label for="completed"
                     value="Recevoir un mail quand une adhésion est complétée"/>
            <x-input id="completed" type="checkbox"
                     wire:model="completed"/>
            <x-input-error for="completed" class="mt-2"/>
        </div>
    </x-slot>

    <x-slot name="actions">
        <x-action-message class="mr-3" on="saved">
            {{ __('Saved.') }}
        </x-action-message>

        <x-button>
            {{ __('Save') }}
        </x-button>
    </x-slot>
</x-form-section>
