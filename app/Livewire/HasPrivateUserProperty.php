<?php

declare(strict_types=1);

namespace App\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait HasPrivateUserProperty
{
    private readonly User $user;

    public function __construct()
    {
        $user = Auth::user();
        if (!$user instanceof User) {
            throw new \RuntimeException("Couldn't find user.");
        }
        $this->user = $user;
    }
}
