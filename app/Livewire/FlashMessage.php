<?php

declare(strict_types=1);

namespace App\Livewire;

use Livewire\Component;

final class FlashMessage extends Component
{
    public string $message;
}
