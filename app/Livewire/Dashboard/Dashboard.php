<?php

declare(strict_types=1);

namespace App\Livewire\Dashboard;

use App\Enums\OptionEnum;
use App\Livewire\HasPrivateUserProperty;
use App\Models\Option;
use Livewire\Attributes\Title;
use Livewire\Attributes\Url;
use Livewire\Component;

#[Title("Gestion des demands d'adhésions")]
final class Dashboard extends Component
{
    use HasPrivateUserProperty;

    #[Url]
    public string $filter = '';

    public function mount(): void
    {
        foreach ([OptionEnum::UNION_BIC, OptionEnum::UNION_IBAN, OptionEnum::WELCOME_SESSION_LINK] as $option) {
            if (Option::get($option)->value === null) {
                abort(
                    503,
                    \sprintf(
                        "Le paramètre '%s' n'est pas défini. Un·e admin doit se rendre sur %s",
                        __("dashboard.settings.{$option->value}"),
                        route('admin.settings')
                    )
                );
            }
        }
    }
}
