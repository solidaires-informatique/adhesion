<?php

declare(strict_types=1);

namespace App\Livewire\Application;

use Livewire\Component;

final class Element extends Component
{
    public string $title;
    public string $content;
}
