<?php

declare(strict_types=1);

namespace App\Mail;

use App\Enums\OptionEnum;
use App\Models\Application;
use App\Models\Option;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

final class WelcomeNotification extends Mailable implements ShouldQueue
{
    use Queueable;
    use SerializesModels;

    public function __construct(
        public readonly Application $application,
    ) {
    }

    public function envelope(): Envelope
    {
        return new Envelope(
            bcc: config('mail.welcome_bcc'),
            replyTo: ['contact@solidairesinformatique.com'],
            subject: 'Bienvenue à Solidaires Informatique',
        );
    }

    public function content(): Content
    {
        return new Content(
            markdown: 'emails.welcome',
            with: [
                'welcome_session_link' => Option::get(OptionEnum::WELCOME_SESSION_LINK)->value,
                'bic' => Option::get(OptionEnum::UNION_BIC)->value,
                'iban' => Option::get(OptionEnum::UNION_IBAN)->value,
            ],
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
