<?php

declare(strict_types=1);

namespace App\Livewire;

use App\Enums\OptionEnum;
use App\Models\Option;
use App\Rules\Iban;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rules\RequiredIf;
use Livewire\Component;

final class SettingsForm extends Component
{
    use HasTextFields;

    /** @var array<string, string> */
    public array $values;

    /** @var Collection<string, Option> */
    private Collection $options;

    public function __construct()
    {
        $this->options = Option::getGlobals();
    }

    public function mount(): void
    {
        $this->values = $this->options
            ->pluck('value', 'key')
            ->toArray();
    }

    public function submit(): void
    {
        $this->validate();

        foreach ($this->values as $key => $value) {
            $option = Option::get(OptionEnum::from($key));
            if ($option->value === $value) {
                continue;
            }

            $option->value = $value;
            $option->update();
            Log::info("Option updated : {$key}.");
        }

        session()->flash('success_message', 'Paramètres mis à jour.');

        $this->redirect(route('admin.settings'));
    }

    /**
     * @return array<string, array<int, string|ValidationRule|RequiredIf>>
     */
    public function rules(): array
    {
        return [
            'values.'.OptionEnum::UNION_IBAN->value => self::getTextValidators(true, [new Iban()]),
            'values.'.OptionEnum::UNION_BIC->value => self::getTextValidators(true, ['alpha_num']),
            'values.'.OptionEnum::WELCOME_SESSION_LINK->value => self::getTextValidators(true, ['url:https']),
        ];
    }
}
