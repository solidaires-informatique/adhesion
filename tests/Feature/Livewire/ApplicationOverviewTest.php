<?php

declare(strict_types=1);

namespace Tests\Feature\Livewire;

use Tests\TestCase;
use Livewire\Livewire;
use App\Livewire\ApplicationOverview;
use PHPUnit\Framework\Attributes\Test;
use Database\Factories\ApplicationFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;

final class ApplicationOverviewTest extends TestCase
{
    use RefreshDatabase;

    #[Test]
    function show_formatted_phone(): void
    {
        $phones = [
            '06 00 00 00 00' => [
                '+33600000000',
                '600000000',
                '06 0000 000 0',
            ],
            '05 36 49 68 18' => [
                '05 36 49 68 18',
                '0536496818',
                '536496818',
                '0033536496818',
                '+33 536496818',
                '+33536496818',
                '+33 5 36 49 68 18',
            ],
            '44536496818' => [ // not a french phone, so not formatted
                '44536496818',
            ],
            '+44536496818' => [ // not a french phone, so not formatted
                '+44536496818',
            ]
        ];

        foreach ($phones as $formatted => $unformatted) {
            foreach ($unformatted as $mobile) {
                Livewire::test(
                    ApplicationOverview::class,
                    ['application' => ApplicationFactory::new(['mobile' => $mobile])->create()])
                    ->assertSee($formatted)
                    ->assertOk();
            }
        }
    }
}
