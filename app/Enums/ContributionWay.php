<?php

declare(strict_types=1);

namespace App\Enums;

enum ContributionWay: string
{
    case SEPA = 'sepa';
    case TRANSFER = 'transfer';
    case CHECK = 'check';
}
