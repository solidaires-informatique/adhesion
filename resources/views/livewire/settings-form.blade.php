<div class="w-full max-w-4xl mx-auto mt-2">
{{--    Page non utilisée --}}
    <header class="mb-2 text-center">
        <h1 class="text-4xl">Paramètres</h1>
    </header>

    @if (session('error'))
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white px-4 py-5 sm:px-6 rounded">
                <p class="text-red-500">
                    {{ session('error') }}
                </p>
            </div>
        </div>
    @endif
    @if (session('error'))
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white px-4 py-5 sm:px-6 rounded">
                <p class="text-red-500">
                    {{ session('error') }}
                </p>
            </div>
        </div>
    @endif

    <div class="mt-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white px-4 py-5 sm:px-6 rounded">
                <form wire:submit="submit" method="POST" class="form">
                    @csrf

                    <div class="form__input-group">
                        <x-forms.admin.setting-input :half="true" :key="\App\Enums\OptionEnum::UNION_IBAN->value" />
                        <x-forms.admin.setting-input :half="true" :key="\App\Enums\OptionEnum::UNION_BIC->value" />
                    </div>

                    <div class="form__input-group">
                        <x-forms.admin.setting-input :key="\App\Enums\OptionEnum::WELCOME_SESSION_LINK->value" />
                    </div>

                    <button type="submit" class="form__submit @error('submit') form__submit_error @enderror">
                        <span>Mettre à jour</span>
                    </button>
                    @error('submit')
                    <p class="form__input-group__element__error">{{ $message }}</p>
                    @enderror
                </form>
            </div>
        </div>
    </div>
</div>
