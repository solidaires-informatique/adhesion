



<!--
Dans le cas d'un rapport de bug expliquez les étapes pour le reproduire (comment vous avez trouvé ce bug).
Dans le cas d'une demande de fonctionnalité, expliquez son but et ce qu'elle apporterait au projet.

N'hésitez pas à inclure des captures d'écrans.
-->
<!--
⚠️
Bien qu'illégale, la répression antisyndicale est une réalité largement répandue dans les entreprises de notre secteur.
Ne supprimez la ligne ci-dessous que si associer votre compte gitlab.com à Solidaires Informatique n'est pas risqué.
-->
/confidential
