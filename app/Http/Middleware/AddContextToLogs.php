<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

final class AddContextToLogs
{
    /**
     * `authenticated` allows us to track down which user took an administrative action for accountability and security.
     * This behavior should be reworked if this platform evolves to have users who do not perform administrative tasks.
     */
    public function handle(Request $request, \Closure $next): Response
    {
        $requestId = Str::random(6);
        $user = $request->user();

        Log::withContext([
            'request-id' => $requestId,
            'authenticated' => $user instanceof User ? $user->id : 'not authenticated',
        ]);

        return $next($request);
    }
}
