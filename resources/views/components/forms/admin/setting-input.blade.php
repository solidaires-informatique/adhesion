<div class="form__input-group__element @if(!empty($half)) form__input-group__element_1/2 @endif">
    <label class="form__input-group__element__label" for={{ $key }}>
        {{ __("dashboard.settings.$key") }}
    </label>
    <input
        id={{ \App\Enums\OptionEnum::WELCOME_SESSION_LINK->value }}
        wire:model={{ "values.$key" }}
        required
        class='form__input-group__element__input @error("values.$key") errored @enderror'
    >
    @error("values.$key") <p class="form__input-group__element__error">{{ $message }}</p> @enderror
</div>
