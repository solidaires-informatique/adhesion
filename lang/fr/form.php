<?php

return [
    'contribution_way_value' => [
        'sepa' => 'Prélèvement automatique',
        'transfer' => 'Virement',
        'check' => 'Chèque',
    ],
    'have_employer' => "J'ai un employeur",
    'send_application' => "Envoyer le bulletin d'adhésion",
    'current_date' => "Le",
    'siret' => [
        'generic' => "Une erreur s'est produite lors de la récupération des informations.",
        '404' => 'Aucun établissement trouvé avec ce numéro SIRET.',
    ],
];
