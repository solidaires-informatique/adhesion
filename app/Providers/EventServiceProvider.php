<?php

declare(strict_types=1);

namespace App\Providers;

use App\Events\ApplicationCompletedByTreasury;
use App\Events\MemberReady;
use App\Events\NewApplication;
use App\Listeners\ApplicationCompletedListener;
use App\Listeners\MemberReadyListener;
use App\Listeners\NewApplicationListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

final class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        NewApplication::class => [
            NewApplicationListener::class,
        ],
        ApplicationCompletedByTreasury::class => [
            ApplicationCompletedListener::class,
        ],
        MemberReady::class => [
            MemberReadyListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
