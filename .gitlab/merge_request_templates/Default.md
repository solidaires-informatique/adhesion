## Résumé

## Détails


<!--
⚠️
Bien qu'illégale, la répression antisyndicale est une réalité largement répandue dans les entreprises de notre secteur.
Nous vous encourageons à utiliser un compte sous pseudonyme pour contribuer à ce code.
Pensez aussi à bien configurer git avec ce pseudo dans ce dossier, par exemple :
git config user.email "nomail"
git config user.name "Super Pseudonyme"
-->
