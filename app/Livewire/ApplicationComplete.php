<?php

declare(strict_types=1);

namespace App\Livewire;

use App\Enums\ApplicationStatus;
use App\Models\Application;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

final class ApplicationComplete extends Component
{
    use HasTextFields;

    public Application $application;

    /**
     * @throws AuthorizationException
     */
    public function complete(): void
    {
        $this->validate();

        $this->authorize('complete', Application::class);
        $this->application->status = ApplicationStatus::COMPLETED;
        $this->application->update();
        Log::notice("Application {$this->application->id} completed.");

        redirect(route('dashboard.index'));
    }

    /**
     * @return array<string, array<int, ValidationRule|string>>
     */
    public function rules(): array
    {
        return [
            'application.first_withdrawal_date' => [ApplicationForm::REQUIRED_IF_SEPA, 'nullable', 'date', 'after_or_equal:tomorrow'],
        ];
    }
}
