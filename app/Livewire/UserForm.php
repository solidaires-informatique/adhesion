<?php

declare(strict_types=1);

namespace App\Livewire;

use App\Actions\Fortify\CreateNewUser;
use App\Enums\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;
use Livewire\Component;

final class UserForm extends Component
{
    use HasTextFields;

    public User $user;
    public bool $creation = false;
    public bool $confirmingResetPassword = false;
    public bool $confirmingDelete = false;

    public function mount(): void
    {
        if (Route::currentRouteName() === 'admin.users.create') {
            $this->creation = true;
            $this->user = new User();
            $this->user->role = Role::BOARD;
        }
    }

    public function submit(): void
    {
        $this->validate();

        if ($this->user->email === '') {
            $this->user->email = null;
        }

        if ($this->creation) {
            $this->createAccount();
        } else {
            $this->updateAccount();
        }

        $this->redirect(route('admin.users'));
    }

    public function resetPassword(): void
    {
        // Disconnect the user
        $this->user->remember_token = null;
        $this->user->save();

        if ($this->user->email !== null) {
            $this->sendResetPassword();
            $this->redirect(route('admin.users'));

            return;
        }

        $password = $this->setRandomPassword();
        $this->user->save();

        $message = "Le mot de passe du compte {$this->user->pseudo} a été réinitialisé.";
        Log::notice($message, ['account' => $this->user->id]);
        session()->flash('success_message', $message.' '.$this->getPasswordInstruction($password));

        $this->redirect(route('admin.users'));
    }

    public function delete(): void
    {
        $this->user->delete();

        $message = "Le compte {$this->user->pseudo} a été supprimé.";
        Log::notice($message, ['account' => $this->user->id]);
        session()->flash('success_message', $message);

        $this->redirect(route('admin.users'));
    }

    /**
     * @return array<string, mixed>
     */
    protected function rules(): array
    {
        return [
            'user.pseudo' => [...CreateNewUser::RULES['pseudo'], Rule::unique('users', 'pseudo')->ignore($this->user->id)],
            'user.email' => [...CreateNewUser::RULES['email'], Rule::unique('users', 'email')->ignore($this->user->id)],
            'user.role' => ['required', new Enum(Role::class)],
        ];
    }

    private function updateAccount(): void
    {
        if ($this->user->isDirty('role') && $this->user->role === Role::INACTIVE) {
            // When disabling an account: reset it's password and deconnect the user.
            $this->setRandomPassword();
            $this->user->remember_token = null;
        }
        $this->user->save();

        $message = "Le compte {$this->user->pseudo} a été mis à jour.";
        Log::notice($message, ['account' => $this->user->id]);
        session()->flash('success_message', $message);
    }

    private function createAccount(): void
    {
        $password = $this->setRandomPassword();
        $this->user->save();

        $message = "Le compte {$this->user->pseudo} a été créé.";
        Log::notice($message, ['account' => $this->user->id]);

        if ($this->user->email !== null) {
            $this->sendResetPassword();

            return;
        }

        session()->flash('success_message', $message.' '.$this->getPasswordInstruction($password));
    }

    private function sendResetPassword(): void
    {
        $broker = Password::broker(config('fortify.passwords'));

        $status = $broker->sendResetLink(['email' => $this->user->email]);
        $success = $status === Password::RESET_LINK_SENT;

        $message = $success ? "Mail de réinitialisation de mot de passe envoyé à {$this->user->email}." : __($status);

        Log::notice($message, ['from' => __CLASS__.'::sendResetPassword', 'status' => $status, 'account' => $this->user->id]);

        session()->flash($success ? 'success_message' : 'error_message', $message);
    }

    private function setRandomPassword(): string
    {
        $password = Str::random();
        $this->user->password = Hash::make($password);

        return $password;
    }

    private function getPasswordInstruction(string $password): string
    {
        $profileUrl = route('profile.show');

        return "Mot de passe : {$password} (à changer sur {$profileUrl})";
    }
}
