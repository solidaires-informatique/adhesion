<div class="form__section">
    <h2 class="form__section__instruction">
        Récapitulatif de vos informations
    </h2>
    <p>Si vous souhaitez corriger une information, utilisez le bouton retour ci-dessous.</p>

    <livewire:application-overview :application="$this->application" :showRestrictedInformation="true" />

    <div class="form__input-group">
        <div class="form__input-group__element">
            <label class="form__input-group__element__label" for="applicant_comment">
                {{ __('validation.attributes.comment') }} <span class="text-sm">(optionnel)</span>
            </label>
            <p>
                Laissez un commentaire pour celles et ceux qui traitent les demandes d'adhésion.
            </p>
            <textarea
                wire:model.blur="application.applicant_comment"
                name="applicant_comment"
                maxlength="500"
                id="applicant_comment"
                class="form__input-group__element__input @error('application.applicant_comment') errored @enderror"
            ></textarea>
            <p class="form__input-group__element__instruction">
                Vous pouvez poser une question, décrire ce que vous souhaitez faire au sein du syndicat ou ajouter une précision avec votre adhésion.
            </p>
            @error('application.applicant_comment') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>
    </div>
</div>

@if($application->contribution_way === \App\Enums\ContributionWay::CHECK)
    <div class="form__section">
        <p>Pense à envoyer ta cotisation par chèque à :</p>
        <p>Solidaires Informatique, 31 rue de la Grange aux Belles 75010 Paris</p>
    </div>
@endif

<div class="form__section">
    <div class="form__input-group">
        <div class="form__input-group__element">
            <label class="block tracking-wide">
                <input wire:model.live="application.mail_local" class="mr-2 leading-tight" type="checkbox">
                <span>
                    Je souhaite être inscrit·e sur la liste mail territoriale Solidaires Informatique de ma zone de résidence
                </span>
            </label>
        </div>
    </div>

    <div class="form__input-group">
        <div class="form__input-group__element">
            <label class="block tracking-wide mb-2">
                <input  wire:model.live="terms" class="mr-2 leading-tight" type="checkbox">
                <span>
                    En cochant cette case, je consens à l’utilisation par le syndicat des données personnelles.
                    Celles-ci sont utilisées conformément à l’objet exclusif statutaire du syndicat et dans le respect de la réglementation en vigueur applicable au traitement de données à caractère personnel et, en particulier, le règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016.
                    Vous pouvez exercer votre droit de communication, de modification et de retrait de ces informations de ce présent consentement par demande écrite à l’adresse mail ou postale de contact indiquée sur le bulletin d'adhésion en PDF.
            </span>
            </label>
            @error('terms') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>
    </div>
</div>
