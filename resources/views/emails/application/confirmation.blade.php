<x-mail::message>
Bonjour, nous avons bien reçu ta demande d'adhésion.

Nous la traiterons dans les meilleurs délais.
Tu vas recevoir un e-mail lorsque ton adhésion aura été approuvée.

Si ta situation nécessite une aide d'urgence, contacte nous par mail à [contact@solidairesinformatique.org](mailto:contact@solidairesinformatique.org)

Solidaires Informatique
</x-mail::message>
