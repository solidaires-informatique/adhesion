<?php

namespace Database\Seeders;

use App\Enums\OptionEnum;
use App\Enums\Role;
use App\Models\Option;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

final class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::factory()->create([
            'pseudo' => 'admin',
            'email' => 'admin@example.com',
            'role' => Role::ADMIN,
        ]);
        Option::factory()->create([
            'value' => '1',
            'key' => OptionEnum::READY_RECAP->value,
            'user_id' => $admin->id,
        ]);

        $board = User::factory()->create([
            'pseudo' => 'board',
            'email' => 'board@example.com',
            'role' => Role::BOARD,
        ]);
        Option::factory()->create([
            'value' => '1',
            'key' => OptionEnum::NEW_RECAP->value,
            'user_id' => $board->id,
        ]);
        Option::factory()->create([
            'value' => '1',
            'key' => OptionEnum::COMPLETED_RECAP->value,
            'user_id' => $board->id,
        ]);

        $treasury = User::factory()->create([
            'pseudo' => 'treasury',
            'email' => 'treasury@example.com',
            'role' => Role::TREASURY,
        ]);
        Option::factory()->create([
            'value' => '1',
            'key' => OptionEnum::VALIDATED_RECAP->value,
            'user_id' => $treasury->id,
        ]);

        User::factory()->create([
            'pseudo' => 'inactive',
            'role' => Role::INACTIVE,
        ]);

        User::factory(3)->create();
    }
}
