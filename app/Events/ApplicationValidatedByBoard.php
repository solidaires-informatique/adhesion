<?php

declare(strict_types=1);

namespace App\Events;

use App\Models\Application;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

final readonly class ApplicationValidatedByBoard
{
    use Dispatchable;
    use SerializesModels;

    public function __construct(
        public Application $application,
    ) {
    }
}
