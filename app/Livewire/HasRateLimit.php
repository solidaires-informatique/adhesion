<?php

declare(strict_types=1);

namespace App\Livewire;

use Illuminate\Support\Facades\RateLimiter;

trait HasRateLimit
{
    private function triedLessThanAMinuteAgo(string $rateLimiterKey): bool
    {
        if (RateLimiter::tooManyAttempts($rateLimiterKey, 1)) {
            $seconds = RateLimiter::availableIn($rateLimiterKey);
            $this->addError('submit', __('validation.throttled_with_seconds', ['seconds' => $seconds]));

            return true;
        }

        RateLimiter::hit($rateLimiterKey);

        return false;
    }
}
