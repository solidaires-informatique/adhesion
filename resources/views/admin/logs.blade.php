<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Logs
        </h2>
    </x-slot>


    <div class="overflow-hidden bg-white shadow sm:rounded-lg">
        <div class="border-t border-gray-200">
            @php($i = 0)
            @foreach($logs as $key => $log)
                <div class="@if($i % 2) bg-gray-50 @endif px-4 pb-5 pt-6 sm:px-6">
                    @if($log->context->in)
                    <details>
                        <summary>
                    @endif
                            <span class="@if($log->level === 'error') text-red-500 @endif">[{{ $log->level }}]</span> {{ $log->getOriginal('date') }} -
                                {{ $log->context->message }}
                    @if($log->context->in)
                            at {{ $log->context->in }}:{{ $log->context->line }}
                        </summary>
                        <pre class="text-gray-900">{{ $log->getOriginal('stack_traces') }}</pre>
                    </details>
                    @endif
                </div>
                @php(++$i)
            @endforeach
        </div>
    </div>
</x-app-layout>
