<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body>
        <header class="mt-2 mx-auto">
            <div class="flex justify-around">
                <h1>
                    <span class="text-4xl block">Adhérez en ligne</span>
                    <span class="text-2xl block">Ensemble, on est plus fort·es !</span>
                </h1>
                <img class="px-6 max-w-xs hidden md:block" src="/logo.svg" alt="Solidaires Informatique, Union syndicale Solidaires">
            </div>
        </header>

        @if(session('success_message'))
            <livewire:flash-message :message="session('success_message')" />
        @endif

        <main class="font-sans antialiased mt-2">
            {{ $slot }}
        </main>

        <footer class="mt-6 py-6 mx-auto text-center bg-si-blue-dark text-white">
            <p>
                Si votre situation nécessite une aide d'urgence, contactez nous par mail :
                contact<span id="replace-arobase">[arobase]</span>solidairesinformatique<span id="replace-point">[point]</span>org
            </p>

            <p>
                <a class="light-link" href="https://solidairesinformatique.org">Retourner sur le site</a>
                 - <a href="https://gitlab.com/solidaires-informatique/adhesion" class="light-link">Voir le code source</a>
            </p>
        </footer>

        <noscript>
            <div class="absolute top-0 bg-si-blue-dark text-white text-xl w-full p-4 text-center">
                <p>Pour utiliser le formulaire d'adhésion en ligne, veuillez activer JavaScript.</p>
                <p>Sinon, vous pouvez utiliser le <a class="light-link" href="/bulletin-adhesion-pdf">bulletin en PDF</a>.</p>
            </div>
        </noscript>
        <script async defer>
            document.getElementById('replace-arobase').innerText = '@';
            document.getElementById('replace-point').innerText = '.';
        </script>
        @livewireScripts
    </body>
</html>
